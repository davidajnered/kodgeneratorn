# Kodgeneratorn

Kodgeneratorn är en lösenordsgenerator som hjälper dig att skapa starka lösenord med svenska ord. Genom att generera lösenord med slumpmässigt valda ord som sätts ihop till en mening blir det lätt för dig att komma ihåg dina lösenord.

Kodgeneratorn använder svenska ord för att skapa unika fraser som är lätta att komma ihåg. Fraserna baseras på olika ordklasser. I dagsläget finns det 393 verb, 3249 adjektiv och 401 substantiv, det blir riktigt många unika lösenord som kan skapas. Om du väljer att skapa ett lösenord med endast ett ord använder jag en tabell som innehåller 119142 unika ord.

Styrkan på lösenorden räknas ut med hjälp av zxcvbn och en entropiuträkning. zxcvbn gör ett bra jobb att analysera återkommande mönster hos lösenord och jämför även lösenord mot en engelsk ordbok, men eftersom kodgeneratorn använder svenska ord med svenska tecken används även en egen entropiuträkning som tar hänsyn till det. Jag har även gjort några egna antaganden som att lösenord med endast ett ord alltid är lättgissat. Lösenorden kollas även mot haveibeenpwned för att upptäcka om det har förekommit i någon läcka. I så fall visas en varning.

# Om projektet
Tjänsten är byggd i Laravel med en Vue-komponent för frontend som genererar lösenorden. För att sätta upp projektet lokalt behöver du.

* Maila mig och be mig om databasen med alla ord.
* Skapa en databas lokalt och importera.
* Klona koden.
* Kör `composer install`.
* Kör `yarn install` och `yarn watch` för att bygga js bundle.
* Fixa en api-nyckel från haveibeenpwned.com.
* Kopiera .env.example till .env och lägg in alla uppgifter.

Det bör vara allt. Happy coding!