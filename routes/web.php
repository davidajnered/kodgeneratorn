<?php

use App\Http\Controllers\ApiController;
use App\Http\Controllers\Controller;
use App\Http\Controllers\SystemController;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Route;

/**
 * Route namespaces are used for IndexNow logic.
 */

Route::group(['namespace' => 'public'], function() {
  Route::get('/', [Controller::class, 'frontpage'])->name('frontpage');
  Route::get('/testa-ditt-losenord', [Controller::class, 'testYourPassword'])->name('test_your_password');
  Route::get('/vad-ar-sakerhet', [Controller::class, 'whatIsSecurity'])->name('what_is_security');
  Route::get('/vad-ar-ett-starkt-losenord', [Controller::class, 'whatIsAStrongPassword'])->name('what_is_a_strong_password');
  Route::get('/hur-skapas-losenorden', [Controller::class, 'howAreThePasswordsCreated'])->name('how_are_the_passwords_created');
  Route::get('/integrationer-och-plugins', [Controller::class, 'plugins'])->name('integrations_and_plugins');
  Route::get('/utveckling', [Controller::class, 'development'])->name('development');
  Route::get('/api-dokumentation', [Controller::class, 'apiDocumentation'])->name('api_documentation');

  // Old API routes, redirect to new.
  Route::get('/generate', function() {
    return Redirect::to('/api/generate', 301);
  });

  Route::post('/analyse', function() {
    return Redirect::to('/api/analyse', 301);
  });
});

Route::group(['namespace' => 'api', 'prefix' => 'api'], function() {
  Route::get('/generate', [ApiController::class, 'generate'])->name('api.generate');
  Route::post('/analyse', [ApiController::class, 'analyse'])->name('api.analyse');
});


// System routes
Route::group(['namespace' => 'system'], function() {
  Route::get('/system/import', [SystemController::class, 'import'])->name('system.import');
  Route::get('/system/indexnow', [SystemController::class, 'indexNow'])->name('system.index_now');
  Route::get('/indexnow.txt', function() {
    return response(config('services.indexnow.api_key'), 200, [
      'Content-Type' => 'application/txt'
    ]);
  });
});