<?php

namespace Tests\Unit\Models;

use App\Models\WordProcessor;
use App\Models\GeneratorOptions;
use App\Models\WordRepository as WordRepository;
use App\Services\PasswordGeneratorService;
use App\Services\SystemService;
use Exception;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;

class WordRepositoryTooFewWordsExceptionTest extends TestCase
{
  /**
   * @var PasswordGeneratorService
   */
  private $passwordGeneratorService;

  /**
   * Setup.
   *
   * @return void
   */
  protected function setUp(): void
  {
    parent::setUp();

    Storage::disk('local')->put('testing/database.sqlite', '');
    App::make(SystemService::class)->importFilesToDb(subPath: 'testing/exceptions/too-few-words-exception');

    $wordProcessor = new WordProcessor();
    $wordRepository = new WordRepository();
    $this->passwordGeneratorService = new PasswordGeneratorService($wordProcessor, $wordRepository);
  }

  /**
   * @test
   */
  public function wordRepositoryTooFewWordsException()
  {
    $options = new GeneratorOptions(['number_of_words' => 5]);
    $passwordGeneratorResult = $this->passwordGeneratorService->generate($options);

    $this->assertEquals(true, $passwordGeneratorResult->hasError());
    $this->assertInstanceOf(Exception::class, $passwordGeneratorResult->getError());
  }

}