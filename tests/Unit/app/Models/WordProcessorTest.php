<?php

namespace Tests\Unit\Models;

use App\Helpers\StringAnalyserHelper;
use App\Models\WordProcessor;
use App\Models\WordRepository;
use App\Services\SystemService;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;

class WordProcessorTest extends TestCase
{
  /**
   * Setup.
   *
   * @return void
   */
  protected function setUp(): void
  {
    parent::setUp();

    Storage::disk('local')->put('testing/database.sqlite', '');
    App::make(SystemService::class)->importFilesToDb();
  }

  /**
   * @test
   */
  public function removeSwedishCharsFromCollection()
  {
    $wordProcessor = new WordProcessor();
    $words = collect(['Ångad', 'Älg', 'Övade']);
    $words = $wordProcessor->removeSwedishCharsFromCollection($words);
    $this->assertEquals('Angad', $words[0]);
    $this->assertEquals('Alg', $words[1]);
    $this->assertEquals('Ovade', $words[2]);
  }

  /**
   * @test
   */
  public function removeSwedishCharsFromString()
  {
    $wordProcessor = new WordProcessor();
    $string = $wordProcessor->removeSwedishCharsFromString('åäöÅÄÖ');
    $this->assertEquals('aaoAAO', $string);
  }

  /**
   * @test
   */
  public function stringInsert()
  {
    $wordProcessor = new WordProcessor();

    $string = $wordProcessor->stringInsert('test', 's', 4);
    $this->assertEquals('tests', $string);

    $string = $wordProcessor->stringInsert('test', 's', 3);
    $this->assertEquals('tesst', $string);

    $string = $wordProcessor->stringInsert('test', 's', 0);
    $this->assertEquals('stest', $string);
  }

  /**
   * @test
   */
  public function specialCharsBetweenWords()
  {
    $wordProcessor = new WordProcessor();
    $numberOfWords = 3;
    $numberOfSpecialChars = 2;
    $words = $this->getWords($numberOfWords);
    $wordsAsString = $words->join('');
    $string = $wordProcessor->specialCharsBetweenWords($wordsAsString, clone $words, $numberOfWords, $numberOfSpecialChars);
    $expectedLength = strlen($words->join('')) + $numberOfSpecialChars;
    $this->assertEquals($expectedLength, strlen($string));
  }

  /**
   * @test
   */
  public function specialChars()
  {
    $wordProcessor = new WordProcessor();
    $numberOfWords = 3;
    $words = $this->getWords($numberOfWords);
    $wordsAsString = $words->join('');

    $string = $wordProcessor->specialChars($wordsAsString, 'start', 1);
    $this->assertEquals(strlen($wordsAsString) + 1, strlen($string));
    $this->assertEquals(substr($string, 1), $wordsAsString);

    $string = $wordProcessor->specialChars($wordsAsString, 'end', 2);
    $this->assertEquals(strlen($wordsAsString) + 2, strlen($string));
    $this->assertEquals(substr($string, 0, -2), $wordsAsString);

    $string = $wordProcessor->specialChars($wordsAsString, 'random', 2);
    $this->assertEquals(strlen($wordsAsString) + 2, strlen($string));
  }

  /**
   * @test
   */
  public function numbersBetweenWords()
  {
    $wordProcessor = new WordProcessor();
    $numberOfWords = 3;
    $numberOfNumbers = 2;
    $words = $this->getWords($numberOfWords);
    $wordsAsString = $words->join('');
    $string = $wordProcessor->numbersBetweenWords($wordsAsString, clone $words, $numberOfWords, $numberOfNumbers);
    $expectedLength = strlen($words->join('')) + $numberOfNumbers;
    $this->assertEquals($expectedLength, strlen($string));
  }

  /**
   * @test
   */
  public function numbers()
  {
    $wordProcessor = new WordProcessor();
    $numberOfWords = 3;
    $words = $this->getWords($numberOfWords);
    $wordsAsString = $words->join('');

    $string = $wordProcessor->numbers($wordsAsString, 'start', 1);
    $this->assertEquals(strlen($wordsAsString) + 1, strlen($string));
    $this->assertEquals(substr($string, 1), $wordsAsString);

    $string = $wordProcessor->numbers($wordsAsString, 'end', 2);
    $this->assertEquals(strlen($wordsAsString) + 2, strlen($string));
    $this->assertEquals(substr($string, 0, -2), $wordsAsString);

    $string = $wordProcessor->numbers($wordsAsString, 'random', 2);
    $this->assertEquals(strlen($wordsAsString) + 2, strlen($string));
  }

  /**
   * @test
   */
  public function capitalize()
  {
    $wordProcessor = new WordProcessor();
    $numberOfWords = 3;
    $words = $this->getWords($numberOfWords);
    $wordsAsString = $words->join('');

    $string = $wordProcessor->capitalize($words, $wordsAsString, 'word', $numberOfWords);
    $numberOfCaptitalLetters = StringAnalyserHelper::stringDifference(mb_strtolower($string), $string);
    $this->assertEquals($numberOfWords, $numberOfCaptitalLetters);

    $string = $wordProcessor->capitalize($words, $wordsAsString, 'random', $numberOfWords);
    $numberOfCaptitalLetters = StringAnalyserHelper::stringDifference(mb_strtolower($string), $string);
    $this->assertEquals($numberOfWords, $numberOfCaptitalLetters);

    $string = $wordProcessor->capitalize($words, $wordsAsString, null, $numberOfWords);
    $numberOfCaptitalLetters = StringAnalyserHelper::stringDifference(mb_strtolower($string), $string);
    $this->assertEquals(0, $numberOfCaptitalLetters);
  }

  /**
   * Helper function to get collection of words.
   *
   * @return Collection
   */
  private function getWords()
  {
    $wordRepository = new WordRepository();
    return $wordRepository->get(3);
  }
}
