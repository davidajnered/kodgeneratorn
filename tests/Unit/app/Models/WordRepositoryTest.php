<?php

namespace Tests\Unit\Models;

use App\Models\WordRepository;
use App\Services\SystemService;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;

class WordRepositoryTest extends TestCase
{
  /**
   * Setup.
   *
   * @return void
   */
  protected function setUp(): void
  {
    parent::setUp();

    Storage::disk('local')->put('testing/database.sqlite', '');
    App::make(SystemService::class)->importFilesToDb();
  }

  /**
   * @test
   */
  public function getPassword()
  {
    $wordRepository = new WordRepository();

    $words = $wordRepository->get(1);
    $this->assertTrue(is_a($words, Collection::class));
    $this->assertCount(1, $words);
    foreach ($words as $word) {
        $this->assertGreaterThanOrEqual(12, strlen($word)); // Test fallback
    }

    $minWordLength = 25;
    $words = $wordRepository->get(1, $minWordLength);
    foreach ($words as $word) {
        $this->assertGreaterThanOrEqual($minWordLength, strlen($word));
    }

    $words = $wordRepository->get(2);
    $this->assertCount(2, $words);

    $minWordLength = 9;
    $words = $wordRepository->get(3);
    $this->assertCount(3, $words);

    $minWordLength = 15;
    $words = $wordRepository->get(4);
    $this->assertCount(4, $words);

    $minWordLength = 20;
    $words = $wordRepository->get(5);
    $this->assertCount(5, $words);
  }
}
