<?php

namespace Tests\Unit\Livewire;

use App\Services\SystemService;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Storage;
use Livewire\Livewire;
use Tests\TestCase;

class PasswordGeneratorTest extends TestCase
{
  /**
   * Setup.
   *
   * @return void
   */
  protected function setUp(): void
  {
    parent::setUp();

    Storage::disk('local')->put('testing/database.sqlite', '');
    App::make(SystemService::class)->importFilesToDb();
  }

  /**
   * @test
   */
  public function showFrontpage()
  {
    $this->get(route('frontpage'))
      ->assertStatus(200);
  }

  /**
   * @test
   */
  public function rendersLivewireComponent()
  {
    $this->get(route('frontpage'))
     ->assertSeeLivewire('password-generator');
  }

  /**
   * @test
   */
  public function useNumbers()
  {
    Livewire::test('password-generator')
      ->set('numberOfNumbers', 1)
      ->assertSee('Antal siffror');
  }

  /**
   * @test
   */
  public function generateNewPassword()
  {
    $component = Livewire::test('password-generator');
    $password = $component->password;
    $component->call('generateNewPassword');
    $newPassword = $component->password;
    $this->assertNotEquals($password, $newPassword);
  }

}