<?php

namespace Tests\Unit\Livewire;

use App\Services\SystemService;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Storage;
use Livewire\Livewire;
use Tests\TestCase;

class PasswordAnalyserTest extends TestCase
{
  /**
   * Setup.
   *
   * @return void
   */
  protected function setUp(): void
  {
    parent::setUp();

    Storage::disk('local')->put('testing/database.sqlite', '');
    App::make(SystemService::class)->importFilesToDb();
  }

  /**
   * @test
   */
  public function showFrontpage()
  {
    $this->get(route('test_your_password'))
      ->assertStatus(200);
  }

  /**
   * @test
   */
  public function rendersLivewireComponent()
  {
    $this->get(route('test_your_password'))
     ->assertSeeLivewire('password-analyser');
  }

  /**
   * @test
   */
  public function analysePassword()
  {
    Livewire::test('password-analyser')
      ->set('password', 'password')
      ->call('analysePassword')
      ->assertSee('Varning!');
  }

  /**
   * @test
   */
  public function analyseEmptyPassword()
  {
    Livewire::test('password-analyser')
      ->set('password', '')
      ->call('analysePassword')
      ->assertSee('Ange ett lösenord.');
  }

}