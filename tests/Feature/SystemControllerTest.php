<?php

namespace Tests\Feature;

use App\Services\SystemService;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class SystemControllerTest extends TestCase
{
  use RefreshDatabase;

  /**
   * @test
   */
  public function import()
  {
    $response = $this->get(route('system.import'));
    $response->assertStatus(200);
    $results = json_decode($response->getContent());

    foreach ($results as $result) {
      $this->assertEquals(false, $result->errors);
    }
  }

  /**
   * @test
   */
  public function indexNow()
  {
    $response = $this->get(route('system.index_now'));
    $response->assertStatus(200);
  }

  /**
   * @test
   */
  public function pingToIndexNow()
  {
    $responses = app()->make(SystemService::class)->pingIndexNow();

    foreach($responses as $searchEngine => $response) {
      $this->assertEquals(403, $response); // Requests should not work locally.
    }
  }
}
