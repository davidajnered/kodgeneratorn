<?php

namespace Tests\Feature;

use App\Services\SystemService;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;

class ControllerTest extends TestCase
{
  use RefreshDatabase;

  /**
   * Setup.
   *
   * @return void
   */
  protected function setUp(): void
  {
    parent::setUp();

    Storage::disk('local')->put('testing/database.sqlite', '');
    App::make(SystemService::class)->importFilesToDb();
  }

  /**
   * @test
   */
  public function frontpage()
  {
    $response = $this->get(route('frontpage'));
    $response->assertStatus(200);
  }

  /**
   * @test
   */
  public function testYourPassword()
  {
    $response = $this->get(route('test_your_password'));
    $response->assertStatus(200);
  }

  /**
   * @test
   */
  public function whatIsSecurity()
  {
    $response = $this->get(route('what_is_security'));
    $response->assertStatus(200);
  }

  /**
   * @test
   */
  public function whatIsAStrongPassword()
  {
    $response = $this->get(route('what_is_a_strong_password'));
    $response->assertStatus(200);
  }

  /**
   * @test
   */
  public function howAreThePasswordsCreated()
  {
    $response = $this->get(route('how_are_the_passwords_created'));
    $response->assertStatus(200);
  }

  /**
   * @test
   */
  public function plugins()
  {
    $response = $this->get(route('integrations_and_plugins'));
    $response->assertStatus(200);
  }

  /**
   * @test
   */
  public function development()
  {
    $response = $this->get(route('development'));
    $response->assertStatus(200);
  }

  /**
   * @test
   */
  public function apiDocumentation()
  {
    $response = $this->get(route('api_documentation'));
    $response->assertStatus(200);
  }

}
