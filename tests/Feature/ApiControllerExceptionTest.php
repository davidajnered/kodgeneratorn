<?php

namespace Tests\Feature;

use App\Services\SystemService;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;

class ApiControllerExceptionTest extends TestCase
{
  use RefreshDatabase;

  /**
   * Setup.
   *
   * @return void
   */
  protected function setUp(): void
  {
    parent::setUp();

    Storage::disk('local')->put('testing/database.sqlite', '');
    App::make(SystemService::class)->importFilesToDb('test'); // Invalid path
  }

  /**
   * @test
   */
  public function generatePasswordException()
  {
    $response = $this->get(route('api.generate'));
    $response->assertStatus(400);
  }

  /**
   * @test
   */
  public function analysePasswordException()
  {
    $response = $this->post(route('api.analyse'));
    $response->assertStatus(400);
  }

}
