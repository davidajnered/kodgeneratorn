<?php

namespace Tests\Feature;

use App\Services\SystemService;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;

class ApiControllerTest extends TestCase
{
  use RefreshDatabase;

  /**
   * Setup.
   *
   * @return void
   */
  protected function setUp(): void
  {
    parent::setUp();

    Storage::disk('local')->put('testing/database.sqlite', '');
    App::make(SystemService::class)->importFilesToDb();
  }

  /**
   * @test
   */
  public function apiResponseStructure()
  {
    $response = $this->get(route('api.generate'));
    $response->assertStatus(200)
    ->assertJsonStructure([
      'source',
      'generated',
      'analysis' => [
        'length',
        'strength_explanations',
        'zxcvbn',
        'entropy',
        'breached',
        'strength_explanation' => [
          'header',
          'body',
        ]
      ]
    ]);
  }

  /**
   * @test
   */
  public function analyse()
  {
    $response = $this->post(route('api.analyse'), ['password' => 'password']);
    $response->assertStatus(200)
    ->assertJsonStructure([
      'length',
      'strength_explanations',
      'strength_explanation',
      'zxcvbn',
      'entropy',
      'breached'
    ]);
  }
}
