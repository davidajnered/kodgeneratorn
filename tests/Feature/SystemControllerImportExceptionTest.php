<?php

namespace Tests\Feature;

use App\Services\SystemService;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;

class SystemControllerImportExceptionTest extends TestCase
{
  use RefreshDatabase;

  /**
   * @var Collection
   */
  private $results;

  /**
   * Setup.
   *
   * @return void
   */
  protected function setUp(): void
  {
    parent::setUp();

    Storage::disk('local')->put('testing/database.sqlite', '');
    $this->results = App::make(SystemService::class)->importFilesToDb('test'); // Invalid path
  }

  /**
   * @test
   */
  public function importExceptionTest()
  {
    $response = $this->get(route('system.import', ['sub_path' => 'test'])); // Invalid path
    $response->assertStatus(500);
  }

}
