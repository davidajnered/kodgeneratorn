document.addEventListener('DOMContentLoaded', function() {
  const header = document.querySelector('header');

  // Header scroll
  const body = document.body;
  const scrollUp = 'scroll-up';
  const scrollDown = 'scroll-down';
  let lastScroll = 0;
  document.addEventListener('scroll', (event) => {
    const currentScroll = window.scrollY;

    if (currentScroll <= 0) {
      body.classList.remove(scrollUp, scrollDown);
      return;
    }

    if (currentScroll > lastScroll && !body.classList.contains(scrollDown)) {
      // Down
      body.classList.remove(scrollUp);
      body.classList.add(scrollDown);
    } else if (currentScroll < lastScroll && body.classList.contains(scrollDown)) {
      // Up
      body.classList.remove(scrollDown);
      body.classList.add(scrollUp);
    }

    lastScroll = currentScroll;
  });
});