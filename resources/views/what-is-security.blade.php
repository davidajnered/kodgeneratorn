@extends('layout', [
  'title' => 'Vad är säkerhet på internet?',
  'description' => 'Det finns många aspekter till säkerhet på internet t.ex. bedrägerier, men här tar vi upp dem som rör lösenord och att skapa konton hos olika tjänster.'
])

@section('content')
  <section>
    <div class="center legible">
      <h1>Säkerhet på internet</h1>
      <p class="preamble">Säkerhet på internet är ett väldigt bredd begrepp och kan handla om allt ifrån starka lösenord, säkra anslutningar, serverar, bedrägerier. Listan kan göras lång...</p>
    </div>
  </section>

  <section class="center legible">
    <h2>Säkra dina konton</h2>
    <p>Du har antagligen flera tjänster på internet och din epost brukar vara en av de viktigaste. Förutom att ha ett starkt lösenord (som vi går igenom på <a href="/vad-ar-ett-starkt-losenord">den här sidan</a>) så finns det en del enkla och smarta saker du kan göra för att du ska bli ännu tryggare.</p>

    <h3>Två-faktor-autentisering</h3>
    <p>2FA (2-faktor-autentisering) eller MFA(multi-faktor-autentisering) innebär att du loggar in med två eller fler "bevis", t.ex. kan det vara ditt lösenord (något du vet) och att du får ett SMS till din telefon (något du har) med en kod som krävs för att du ska komma vidare i inloggningsprocessen. Det kan också var t.ex fingeravtryck (något du är). 2FA är en funktion som många tjänster har idag.</p>

    <h3>Välj en annan epostadress</h3>
    <p>En sätt att göra dina inloggningsuppgifter unika för den tjänst du vill skapa konto hos är att använda en annan epostadress, t.ex. tjänstens-namn@min-domän.se. Fördelen med att göra på det här sättet är att även om tjänsten blir hackad och dina uppgifter läcks så används din epost (och lösenord) endast på den tjänsten, så en hacka kommer inte åt några andra av dina konton.</p>
    <p><i>Den här lösning kräver lite teknisk kunskap, och du behöver ha en egen domän samt veta hur du ställer in din epost. Mer detaljerad information om hur du gör kommer senare.</i></p>

    <h2>Använd en lösenordshanterare</h2>
    <p>Ett annat bra sätt att undvika att skapa svaga lösenord bara för att de är lätta att komma ihåg eller att använda sitt vanliga lösenord ännu en gång är att spara lösenord i en lösenordshanterare. En lösenordshanterare är ett register där du sparar dina inloggningsuppfiter tillsammans med vilken tjänst lösenordet tillhör. När du behöver logga in på tjänsten nästa gång loggar du först in i din lösenordshanterare och kopierar ditt starka lösenord. Det blir ett extra steg om du inte redan är inloggad i din lösenordhanterare, men säkerhet går före bekvämlighet.</p>
    <p>Det finns många lösenordshanterare att välja mellan. Jag använder själv Bitwarden. Här kommer en lista på några tjäster utan inbördes ordning. Jag rekommenderar att du läser på om tjänsterna för att hitta en som passar dig.</p>
    <p>
      <ul>
        <li><a href="https://bitwarden.com" target="_blank">Bitwarden</a></li>
        <li><a href="https://passpack.com" target="_blank">Passpack</a></li>
        <li><a href="https://1password.com" target="_blank">1Password</a></li>
        <li><a href="https://lastpass.com" target="_blank">LastPass</a></li>
      </ul>
    </p>
  </section>
@endsection
