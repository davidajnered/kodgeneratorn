@extends('layout', [
  'title' => 'Utveckling av kodgeneratorn',
  'description' => 'På den här sidan listar jag utvecklingsidéer och önskemål som har kommit in för att både ni och jag ska få en överblick.'
])

@section('content')
  <section>
    <div class="center legible">
      <h1>Utveckling</h1>
      <p>Kodgeneratorn är ett aktivt projekt men eftersom det är ett relativ litet och enkelt projekt släpps det inte nya funktioner hela tiden. Jag är däremot öppen för önskemål så tveka inte att höra av dig om du har någon bra idé på förbättringar.</p>

      <h2>Versionsinformation</h2>
      <h3>#20240808</h3>
      <ul>
        <li>Ändrat färg på knapparna</li>
        <li>Kopiera-knappen fungerar på fler enheter</li>
      </ul>

      <h3>#20240226</h3>
      <ul>
        <li>Lagt till release notes</li>
        <li>Bytt ut Vue.js mot Laravel Livewire</li>
        <li>Förbättrad design</li>
        <li>WCAG-anpassningar</li>
        <li>100% tester av backend (och även delar av frontend)</li>
      </ul>

      <h2>Vidareutveckling</h2>
      <p>Kodgeneratorn är ett aktivt projekt men eftersom det är ett relativ litet och enkelt projekt släpps det inte nya funktioner hela tiden. Jag är däremot öppen för önskemål så tveka inte att höra av dig om du har någon bra idé på förbättringar.</p>

      <ul>
        <li>Tailwind istället för egen CSS</li>
        <li>Göra en tillgänglighetsanalys</li>
        <li>CMS för hantering av innehåll</li>
        <li>Lägga till någon slags statistik (t.ex. antal genererade lösenord)</li>
        <li>Försöka få kodgeneratorn finansierad</li>
        <li>Använda machine learning för lösenordsgenerering</li>
      </ul>
    </div>
  </section>
@endsection