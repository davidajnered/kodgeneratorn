@extends('layout', [
  'title' => 'Hur skapar Kodgeneratorn lösenord?',
  'description' => 'På den här sidan beskrivs mer tekniskt hur kodgeneratorn genererar lösenord.'
])

@section('content')
  <section>
    <div class="center legible">
      <h1>Hur skapas lösenorden?</h1>
      <p>
        Kodgeneratorn använder svenska ord för att skapa unika fraser som är lätta att komma ihåg. Fraserna baseras på olika ordklasser.
        I dagsläget finns det {{ $verbsCount }} verb, {{ $adjectivesCount }} adjektiv och {{ $nounsCount }} substantiv, det blir riktigt många* unika lösenord som kan skapas. Om du väljer att skapa ett lösenord med endast ett ord använder jag en tabell som innehåller {{ $wordsCount }} unika ord**.
      </p>

      <p>
        Lösenorden genereras utifrån olika former beroende på hur många ord du väljer att inkludera, detta för att skapa så tydliga men samtidigt ologiska meningar som du lätt kan komma ihåg. Beroende på vad du gör för val inkluderas även siffor, specialtecken och svenska tecken, antingen slumpmässigt placerade eller placerade på platser i meningen som gör det lättare att komma ihåg lösenordet.
      </p>

      <p>
        När lösenordet är skapat räknas styrkan ut med hjälp av <a href="https://dropbox.tech/security/zxcvbn-realistic-password-strength-estimation">zxcvbn</a> och en entropiuträkning.
        zxcvbn är en algoritm som försöker estimerar ett lösenords styrka utifrån vanligt förekommande mönster, t.ex. qwerty, 123 med fler. zxcvbn matchar även lösenord mot engelska ordböcker vilket kanske inte hjälper kodgeneratorn så mycket eftersom den endast använder svenska ord, men det är kanske relevant för något enstaka låneord. Kodgeneratorn kör alla genererade lösenord genom zxcvbn och använder den poäng som lösenordet får i uppskattningen av lösenordet styrka.
      </p>

      <p>
        Eftersom kodgeneratorn använder svenska ord med svenska tecken används även en egen entropiuträkning som tar hänsyn till det.
        Jag har även gjort några egna antaganden som att lösenord med endast ett ord alltid är lättgissat.
      </p>

      <p>
        Jag tyckte inte att det räckte där utan jag kollar även mot databasen <a href="https://haveibeenpwned.com" target="_blank">haveibeenpwned</a> om lösenordet har dykt upp i tidigare läckor. I så fall går det att anta att lösenordet redan finns i de ordböcker som används för att gissa sig till lösenord. Det är också därför som lösenord med endast ett ord anses vara osäkert, även om det innehåller många tecken.
      </p>

      <p>
        <i class="small">* Så många att de uträkningar jag har försökt göra säger att det finns oändligt många.</i><br>
        <i class="small">** Varning för att vissa ord inte är helt PK. Stöter du på ett olämpligt ord som bör tas bort är du välkommen att höra av dig till mig.</i>
      </p>
    </div>
  </section>
@endsection