<?php

namespace App\Services;

use Exception;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Route;

class SystemService
{
  /**
   * Import files to database.
   *
   * @return void
   */
  public function importFilesToDb($subPath = false)
  {
    $imports = [
      [
        'file' => 'adjectives',
        'table' => 'adjectives',
        'columns' => ['adjective'],
        'query' => 'INSERT INTO adjectives (adjective) VALUES',
      ],
      [
        'file' => 'nouns',
        'table' => 'nouns',
        'columns' => ['singular_indefinite', 'singular_definite', 'plural_indefinite', 'plural_definite'],
        'query' => 'INSERT INTO nouns (singular_indefinite, singular_definite, plural_indefinite, plural_definite) VALUES',
      ],
      [
        'file' => 'verbs',
        'table' => 'verbs',
        'columns' => ['infinitive', 'imperative', 'present', 'preteritum', 'supinum'],
        'query' => 'INSERT INTO verbs (infinitive, imperative, present, preteritum, supinum) VALUES',
      ],
      [
        'file' => 'words',
        'table' => 'words',
        'columns' => ['word'],
        'query' => 'INSERT INTO words (word) VALUES',
      ]
    ];

    $results = new Collection();

    try {
      // Migrate database
      Artisan::call('migrate');

      // Empty tables if they exist.
      DB::table('adjectives')->truncate();
      DB::table('nouns')->truncate();
      DB::table('verbs')->truncate();
      DB::table('words')->truncate();

      foreach ($imports as $import) {
        if ($subPath) {
          $filePath = base_path('storage/app/' . $subPath . '/' . $import['file'] . '.csv');
        } else {
          $filePath = base_path('storage/app/' . $import['file'] . '.csv');
        }

        $rows = array_map('str_getcsv', file($filePath));
        $rows = collect($rows);
        $rows->shift(); // Remove header

        $rows = $rows->map(function($row) {
          return '("' . join('","', $row) . '")';
        });

        $query = 'INSERT INTO ' . $import['table'] . ' (' . join(',', $import['columns']) . ') VALUES ' . join(',', $rows->toArray()) . ';';
        $result = DB::insert($query);

        if ($result) {
          $results->put($import['table'], (object) [
            'rows' => $rows->count(),
            'errors' => false,
            'message' => 'Success',
          ]);
        } else {

        }
      }
    } catch (Exception $e) {
      $results->put($import['table'], (object) [
        'rows_inserted' => 0,
        'errors' => true,
        'message' => $e->getMessage(),
      ]);
    }

    return $results;
  }

  /**
   * Make request to IndexNow.
   *
   * @return void
   */
  public function pingIndexNow()
  {
    $routes = Route::getRoutes()->getRoutes();
    $indexableUrls = [];
    foreach ($routes as $route) {
      if (isset($route->action['namespace']) && $route->action['namespace'] == 'public') {
        $indexableUrls[] = url('') . '/' . str_replace('/', '', $route->uri());
      }
    }

    $searchEngines = [
      'https://bing.com',
    ];

    $responses = [];
    foreach ($searchEngines as $searchEngine) {
      $response = Http::post($searchEngine . '/IndexNow', [
        'host' => url(''),
        'key' => config('services.indexnow.api_key'),
        'keyLocation' => url('') . '/indexnow.txt',
        'urlList' => $indexableUrls
      ]);

      $responses[$searchEngine] = $response->status();
    }

    return $responses;
  }
}