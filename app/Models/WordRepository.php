<?php

namespace App\Models;

use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Collection;

class WordRepository
{
  /**
   * Query database.
   *
   * @param integer $numberOfWords
   * @param integer $minWordLength
   * @param integer $iteration number of tries to get words according to parameters.
   * @return Collection
   */
  public static function get(int $numberOfWords, int $minWordLength = 12, int $iteration = 0)
  {
    $random = 'RAND()';
    $char_length = 'CHAR_LENGTH';

    if (config('database.default') == 'sqlite') {
      $random = 'RANDOM()';
      $char_length = 'LENGTH';
    }

    if ($numberOfWords == 1) {
      $words = DB::select("
        SELECT word FROM words WHERE $char_length(word) >= $minWordLength ORDER BY $random;
      ");
    } else if ($numberOfWords == 2) {
      $words = DB::select("
        SELECT
        (SELECT adjective FROM adjectives ORDER BY $random LIMIT 1) AS 'adjective1',
        (SELECT singular_indefinite FROM nouns ORDER BY $random LIMIT 1) AS 'noun1';
      ");
    } else if ($numberOfWords == 3) {
      $words = DB::select("
        SELECT
        (SELECT adjective FROM adjectives ORDER BY $random LIMIT 1) AS 'adjective1',
        (SELECT singular_indefinite FROM nouns ORDER BY $random LIMIT 1) AS 'noun1',
        (SELECT preteritum FROM verbs ORDER BY $random LIMIT 1) AS 'verb';
      ");
    } else if ($numberOfWords == 4) {
      $words = DB::select("
        SELECT
        (SELECT adjective FROM adjectives ORDER BY $random LIMIT 1) AS 'adjective1',
        (SELECT singular_indefinite FROM nouns ORDER BY $random LIMIT 1) AS 'noun1',
        (SELECT preteritum FROM verbs ORDER BY $random LIMIT 1) AS 'verb',
        (SELECT singular_indefinite FROM nouns ORDER BY $random LIMIT 1) AS 'noun2';
      ");
    } else if ($numberOfWords == 5) {
      $words = DB::select("
        SELECT
        (SELECT adjective FROM adjectives ORDER BY $random LIMIT 1) AS 'adjective1',
        (SELECT singular_indefinite FROM nouns ORDER BY $random LIMIT 1) AS 'noun1',
        (SELECT preteritum FROM verbs ORDER BY $random LIMIT 1) AS 'verb',
        (SELECT adjective FROM adjectives ORDER BY $random LIMIT 1) AS 'adjective2',
        (SELECT singular_indefinite FROM nouns ORDER BY $random LIMIT 1) AS 'noun2';
      ");
    }

    $words = collect($words[0]);

    if ($words->filter()->count() != $numberOfWords || $words->count() !== $words->unique()->count()) {
      if ($iteration >= 3) {
        throw new Exception('Too few words to choose from. Generator failed after 3 attempts.');
      }

      // Recursive function. Run again if duplicates are found.
      $iteration += 1;
      return self::get($numberOfWords, $minWordLength, $iteration);
    } else {
      return $words;
    }
  }
}