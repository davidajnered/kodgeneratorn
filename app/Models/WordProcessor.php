<?php

namespace App\Models;

use App\Helpers\StringAnalyserHelper;
use Illuminate\Support\Collection;

class WordProcessor
{
  /**
   * Remove swedish chars from collection.
   *
   * @param Collection $words
   * @return Collection
   */
  public function removeSwedishCharsFromCollection(Collection $words)
  {
    return $words->map(function($word) {
      return $this->removeSwedishCharsFromString($word);
    });
  }

  /**
   * Remove swedish chars from string.
   *
   * @param string $string
   * @return string
   */
  public function removeSwedishCharsFromString(string $string)
  {
    $removeChars = ['/å/', '/ä/', '/ö/', '/Å/', '/Ä/', '/Ö/'];
    $replaceWith = ['a', 'a', 'o', 'A', 'A', 'O'];

    return preg_replace($removeChars, $replaceWith, $string);
  }

  /**
   * Insert char at a specific position in a string.
   *
   * @param string $string
   * @param string $char
   * @param int $position
   * @return string
   */
  public function stringInsert(string $string, string $char, int $position)
  {
    return mb_substr($string, 0, $position) . $char . mb_substr($string, $position);
  }

  /**
   * Insert special chars between words.
   *
   * @param string $generated
   * @param Collection $words
   * @param integer $numberOfWords
   * @param integer $numberOfSpecialChars
   * @return string
   */
  public function specialCharsBetweenWords(
    string $generated,
    Collection $words,
    int $numberOfWords,
    int $numberOfSpecialChars
  ): string
  {
    $specialChars = str_split(config('kodgeneratorn.special_chars'));
    $splits = $this->splitGeneratedString($generated, $words);

    // Calculate the number
    $numbersPerSplit =  floor($numberOfSpecialChars / ($numberOfWords - 1));
    $rest = $numberOfSpecialChars % ($numberOfWords - 1);

    $splits = $splits->map(function($split, $index) use ($numberOfWords, $numbersPerSplit, $rest, $specialChars) {
      // For the last index we about because we're adding numbers between words, not after.
      if ($index == ($numberOfWords - 1)) {
        return $split;
      }

      $numbers = $numbersPerSplit;

      // For the first index we add $rest (if any)
      if ($index == 0) {
        $numbers += $rest;
      }

      for ($i = 0; $i < $numbers; $i++) {
        $split .= $specialChars[rand(0, count($specialChars) - 1)];
      }

      return $split;
    });

    return $splits->join('');
  }

  /**
   * Insert special chars at start, end or random position.
   *
   * @param string $generated
   * @param string $position
   * @param integer $numberOfSpecialChars
   * @return string
   */
  public function specialChars(string $generated, string $position = null, int $numberOfSpecialChars)
  {
    $specialChars = str_split(config('kodgeneratorn.special_chars'));
    $randomSpecialChars = [];
    for ($index = 0; $index < $numberOfSpecialChars; $index++) {
      $randomSpecialChars[] = $specialChars[rand(0, count($specialChars) - 1)];
    }

    if ($position == 'start') {
      $generated = join('', $randomSpecialChars) . $generated;
    } else if ($position == 'end') {
      $generated = $generated . join('', $randomSpecialChars);
    } else if ($position == 'random' || $position == null) {
      while (count($randomSpecialChars) > 0) {
        $randomChar = array_shift($randomSpecialChars);
        $generated = $this->stringInsert($generated, $randomChar, rand(0, strlen($generated)));
      }
    }

    return $generated;
  }

  /**
   * Handle logic for adding numbers between words.
   *
   * @param string $generated
   * @param Collection $words
   * @param integer $numberOfWords
   * @param integer $numberOfNumbers
   * @return string
   */
  public function numbersBetweenWords(
    string $generated,
    Collection $words,
    int $numberOfWords,
    int $numberOfNumbers
  ): string
  {
    $splits = $this->splitGeneratedString($generated, $words);

    // Calculate the number
    $numbersPerSplit =  floor($numberOfNumbers / ($numberOfWords - 1));
    $rest = $numberOfNumbers % ($numberOfWords - 1);

    $splits = $splits->map(function($split, $index) use ($numberOfWords, $numbersPerSplit, $rest) {
      // For the last index we about because we're adding numbers between words, not after.
      if ($index == ($numberOfWords - 1)) {
        return $split;
      }

      $numbers = $numbersPerSplit;

      // For the first index we add $rest (if any)
      if ($index == 0) {
        $numbers += $rest;
      }

      for ($i = 0; $i < $numbers; $i++) {
        $split .= rand(0, 9);
      }

      return $split;
    });

    return $splits->join('');
  }

  /**
   * Undocumented function
   *
   * @param string $generated
   * @param string $position
   * @param integer $numberOfNumbers
   * @return string
   */
  public function numbers(string $generated, string $position = null, int $numberOfNumbers)
  {
    $randomNumbers = [];
    for ($index = 0; $index < $numberOfNumbers; $index++) {
      $randomNumbers[] = rand(0, 9);
    }

    if ($position == 'start') {
      $generated = join('', $randomNumbers) . $generated;
    } else if ($position == 'end') {
      $generated = $generated . join('', $randomNumbers);
    } else if ($position == 'random' || $position == null) {
      while (count($randomNumbers) > 0) {
        $randomNumber = array_shift($randomNumbers);
        $generated = $this->stringInsert($generated, $randomNumber, rand(0, strlen($generated)));
      }
    }

    return $generated;
  }

  /**
   * Capitalize each word or randomly.
   *
   * @param Collection $words
   * @param string $type
   * @param integer $numberOfWords
   * @return string
   */
  public function capitalize(Collection $words, string $generated, string $type = null, int $numberOfWords)
  {
    if ($type == 'random') {
      // Random
      $originalString = $generated;
      $numberOfCapitalLetters = 0;
      while ($numberOfCapitalLetters < $numberOfWords) {
        $randomIndex = rand(0, strlen($generated) - 1);
        $generated[$randomIndex] = strtoupper($generated[$randomIndex]);
        $numberOfCapitalLetters = StringAnalyserHelper::stringDifference($generated, $originalString);
      }
    } else if ($type == 'word') {
      // Each word
      $words = $words->map(function($word) {
        return mb_convert_case($word, MB_CASE_TITLE);
      });

      $generated = $words->join('');
    } else if ($type == 'first') {
      // First
      $generated = $words->join('');
      $generated = ucfirst($generated);
    } else if ($type == 'last') {
      // Last
      $generated = $words->join('');
      $generated = strrev(ucfirst(strrev($generated)));
    } else {
      $generated = $words->join('');
    }

    return $generated;
  }

  /**
   * Split the generated string back to the original words but keep the modifications made in other functions.
   * This is needed for the randomization of process functions to work since we can't rely on the original words.
   *
   * @param string $generated
   * @param Collection $words
   * @return Collection
   */
  private function splitGeneratedString(string $generated, Collection $words) {
    // Find split positions.
    $splitPositions = [];
    $counter = 0;
    foreach ($words as $word) {
      $splitPositions[] = mb_strpos(mb_strtolower($generated), $word);
    }

    // Perform the split.
    $splittedGenerated = [];
    $counter = 0;
    foreach ($splitPositions as $p) {
      $start = $splitPositions[$counter];
      $end = isset($splitPositions[($counter + 1)]) ? $splitPositions[($counter + 1)] - $start : null;
      $splittedGenerated[] = mb_substr($generated, $start, $end);
      $counter++;
    }

    return collect($splittedGenerated);
  }
}