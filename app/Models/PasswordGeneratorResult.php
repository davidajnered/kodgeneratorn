<?php

namespace App\Models;

use Exception;
use Illuminate\Support\Collection;

class PasswordGeneratorResult
{
  /**
   * Constructor.
   *
   * @param string $password
   * @param Collection $sourceWords
   */
  public function __construct(
    private string|null $password,
    private Collection|null $sourceWords,
    private Exception|null $error = null
  ) { }

  /**
   * Get password.
   *
   * @return string
   */
  public function getPassword(): string
  {
    return $this->password ?? '';
  }

  /**
   * Get words used as the source for the generated password.
   *
   * @return Collection
   */
  public function getSourceWords(): Collection
  {
    return $this->sourceWords ?? new Collection;
  }

  /**
   * Check if result has error.
   *
   * @return boolean
   */
  public function hasError(): bool
  {
    return (bool) $this->error;
  }

  /**
   * Get error
   *
   * @return Exception|null
   */
  public function getError(): Exception|null
  {
    return $this->error;
  }

}