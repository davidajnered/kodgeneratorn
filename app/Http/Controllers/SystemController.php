<?php

namespace App\Http\Controllers;

use App\Services\SystemService;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;

class SystemController extends BaseController
{
  /**
   * Import from file.
   *
   * @return void
   */
  public function import(Request $request)
  {
    $results = app()->make(SystemService::class)->importFilesToDb($request->sub_path);

    $responseCode = 200;
    $results->each(function($result) use (&$responseCode) {
      if ($result->errors) {
        $responseCode = 500;
      }
    });

    return response()->json($results, $responseCode);
  }

  /**
   * Run IndexNow process.
   *
   * @return void
   */
  public function indexNow()
  {
    $responses = app()->make(SystemService::class)->pingIndexNow();

    return response()->json($responses);
  }
}
