<?php

namespace App\Http\Controllers;

use App\Models\GeneratorOptions;
use App\Services\ApiService;
use App\Services\PasswordAnalyserService;
use App\Services\PasswordGeneratorService;
use Exception;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Validator;

class ApiController extends BaseController
{
  use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

  /**
   * API endpoint
   *
   * @param Request $request
   * @return void
   */
  public function generate(Request $request)
  {
    try {
      $options = new GeneratorOptions($request);
      $passwordGeneratorResult = App::make(PasswordGeneratorService::class)->generate($options);
      $passwordAnalysisResult = App::make(PasswordAnalyserService::class)->analyse($passwordGeneratorResult->getPassword(), $options);

      $responseData = App::make(ApiService::class)->getV1ResponseData($passwordGeneratorResult, $passwordAnalysisResult, $options);
      return response()->json($responseData);
    } catch (Exception $e) {
      return response()->json($e->getMessage(), 400);
    }
  }

  /**
   * API endpoint
   *
   * @param Request $request
   * @return void
   */
  public function analyse(Request $request)
  {
    try {
      $analyserResult = app()->make(PasswordAnalyserService::class)->analyse($request->password);

      return response()->json($analyserResult->toArray());
    } catch (Exception $e) {
      return response()->json($e->getMessage(), 400);
    }
  }

}
