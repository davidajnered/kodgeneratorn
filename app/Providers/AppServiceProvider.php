<?php

namespace App\Providers;

use App\Models\WordProcessor;
use App\Models\WordRepository;
use App\Services\ApiService;
use App\Services\PasswordAnalyserService;
use App\Services\PasswordGeneratorService;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(ApiService::class);
        $this->app->singleton(PasswordAnalyserService::class);
        $this->app->singleton(PasswordGeneratorService::class, function ($app) {
            return new PasswordGeneratorService(
                $app->make(WordProcessor::class),
                $app->make(WordRepository::class)
            );
        });
    }
}
