<?php

namespace App\Helpers;

class StringAnalyserHelper
{
  /**
   * Check if a string has any special chars.
   *
   * @param string $string
   * @return boolean
   */
  public static function hasSpecialChars(string $string)
  {
    $specialCharsArray = str_split(config('kodgeneratorn.special_chars'));
    $stringArray = str_split($string);
    $intersect = self::arrayIntersect($stringArray, $specialCharsArray);

    return !empty($intersect) ? count($intersect) : false;
  }

  /**
   * Check if a string has any swedish chars.
   *
   * @param string $string
   * @return boolean
   */
  public static function hasSwedishChars(string $string)
  {
    $specialCharsArray = str_split('åäöÅÄÖ');
    $stringArray = str_split($string);
    $intersect = self::arrayIntersect($stringArray, $specialCharsArray);

    return !empty($intersect) ? count($intersect) : false;
  }

  /**
   * Improved array intersect function that respects duplicates.
   *
   * @param array $array1
   * @param array $array2
   * @return array
   */
  private static function arrayIntersect(array $string, array $specialChars)
  {
    $result = [];
    foreach ($string as $char) {
      if (($key = array_search($char, $specialChars, true)) !== false) {
        $result[] = $char;
      }
    }

    return $result;
  }

  /**
   * Count the difference between two strings
   *
   * @param string $string1
   * @param string $string2
   * @return int
   */
  public static function stringDifference(string $string1, string $string2)
  {
    $diff = array_diff_assoc(
      mb_str_split($string1),
      mb_str_split($string2)
    );

    return count($diff);
  }
}