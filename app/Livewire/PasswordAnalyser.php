<?php

namespace App\Livewire;

use App\Models\PasswordAnalyserResult;
use App\Services\PasswordAnalyserService;
use Exception;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\App;
use Livewire\Component;

class PasswordAnalyser extends Component
{
  /**
   * @var string
   */
  public $password;

  /**
   * @var string
   */
  public $error;

  /**
   * @var PasswordAnalyserResult
   */
  private $passwordAnalysisResult;

  /**
   * Analyse password.
   *
   * @return void
   */
  public function analysePassword()
  {
    try {
      $this->reset(['error']);
      $this->passwordAnalysisResult = App::make(PasswordAnalyserService::class)->analyse($this->password);
    } catch (Exception $e) {
      $this->error = $e->getMessage();
    }
  }

  /**
   * Render.
   *
   * @return View
   */
  public function render(): View
  {
    return view('livewire.password-analyser', [
      'passwordAnalysisResult' => $this->passwordAnalysisResult
    ]);
  }
}